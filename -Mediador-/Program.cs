﻿ using System;

namespace _Mediador_
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");

            //creamos el mediador 
            Mediador miMediador = new Mediador();
            //creamos los colegas 
            ColegaA Ana = new ColegaA("Ana",miMediador);
            ColegaA Luis = new ColegaA("Luis",miMediador);
            ColegaB David = new ColegaB("David", miMediador);

            Ana.Enviar("");
            Luis.Enviar("");
            David.Enviar("");
            Console.WriteLine();

            Console.WriteLine("****verificacion de censura****");
            Luis.Enviar("Las palabrotas no me agradan ");
            Console.WriteLine();


            miMediador.Bloqueo(Luis.Recibir);
            Ana.Enviar("Vean mi Playlist");
            Console.WriteLine();


            miMediador.Suscribir(Luis.Recibir);
            David.Enviar("Me gusta estudiar");

        }
    }
}

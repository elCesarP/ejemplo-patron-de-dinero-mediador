﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _Mediador_
{
    class ColegaA : IColega
    {

        private Mediador mediador;
        private string nombre;



        public ColegaA(string PNombre, Mediador Pmediador)

        {
            //lo asignamos a las variables internas 
            nombre = PNombre;

            mediador = Pmediador;
            //se suscribe 
            mediador.Suscribir(Recibir);    
        }

        
        public void Recibir(String emisor, string mensaje)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Soy {0}, recibí de {1}:{2}", nombre, mensaje) ;
            mediador.Enviar(nombre, mensaje);
        }

        public void Enviar(string mensaje)
        {
            //throw new NotImplementedException();
            //aqui va la logica para enviar un mensaje 
            //no necesariamente debe ser un parametro 
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Soy{0}, envio :{1}",nombre, mensaje);
            mediador.Enviar(nombre, mensaje) ;
        
        
        }

       
    }
}

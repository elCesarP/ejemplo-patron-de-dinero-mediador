﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _Mediador_
{
    class ColegaB:IColega
    {

        private Mediador mediador;
        private string nombre;
        private int contador;
        public ColegaB(string pNombre, Mediador pMediador)
        {
            nombre = pNombre;
            //tenemos   la trasferencia al mediador 
            mediador = pMediador;
            mediador.Suscribir(Recibir);
        
        }


        public void Recibir(string emisor, string mensaje)
        {
            contador++;
            //lleva a cabo la recepcion según su estilo 
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("<-[{0},{1}]({2}): {3}",nombre,emisor,contador,mensaje);
        
        }
        public void Enviar(string mensaje)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("->[{0}]:{1}", nombre, mensaje);
            mediador.Enviar(nombre,mensaje);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _Mediador_
{
    class Mediador
    {
        //colocamos un delegado que usaremos para invocar los metodos 
        public delegate void DEnvio(String emisor, string mensaje);
        DEnvio enviarmensaje;
        //agregamos un metodo a invocar 
        public void Suscribir(DEnvio metodo)
        {
            enviarmensaje+=metodo;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Usted está suscrito uwu");

        }
        public void Enviar(string emisor, string mensaje)
        {
            //usamos un mediador para censurar
            if (mensaje.Contains("palabrota"))
                mensaje = mensaje.Replace("palabrota","**********");

            //enviamos los mensajes correspondiente via delegado
            //invocamos a enviarmensaje pasamos el emisor y mensaje correspondiente 
            enviarmensaje(emisor, mensaje);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("MENSAJE ENVIADO ");
        
        }
        //Si se desea podriamos bloquear a alguien de la lista con un metodo bloqueo
        public void Bloqueo(DEnvio metodo)
        {
            enviarmensaje -= metodo;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Bloqueadoo");
        }


    }
}
